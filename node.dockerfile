FROM debian:buster

RUN apt update && apt install -y wget \
    nano \
    net-tools\
    curl\
    git

RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt install -y nodejs

RUN npm install -g sass
CMD [ "sass --watch --poll scss:css" ]

WORKDIR /home
