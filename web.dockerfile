FROM debian:buster

EXPOSE 80 443 

RUN apt update && apt install -y wget \
    nano \
    net-tools\
    curl\
    git

RUN apt update && apt install -y apache2 \
php

RUN echo ServerName mygatito.com.br >> /etc/apache2/apache2.conf
WORKDIR /var/www/html
