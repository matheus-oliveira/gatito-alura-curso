
## Authors

- [@Matheus Oliveira](https://gitlab.com/matheus-oliveira/gatito-alura-curso)
 #### Inicie os containers
```
docker-compose -f "compose.yml" up -d --build
```

# Tecnologias e suas verções
|Technologia|Versão|
|------|--|
|NodeJs|16|

# Notas da atualização
### Instalasão do Sass global
```
npm install -g sass
```
### Compilar o sass
    
```
sass <arquivo.sass>:<arquivo css>
sass <diretorio sass>:<diretorio css>
```
    
Argumento "--watch"  faz com que a compilação seja em tempo real
```
sass --watch <arquivo.sass>:<arquivo.css>
sass --watch <diretorio sass>:<diretorio css>
```
O argumento "--poll" deve ser usado quando usar Docker ou unidade remota
    
```
sass --watch --poll <arquivo.sass>:<arquivo.css>
sass --watch --poll <diretorio sass>:<diretorio css>
```


#### Arquivo de reset para o css

```
https://meyerweb.com/eric/tools/css/reset/reset.css
```